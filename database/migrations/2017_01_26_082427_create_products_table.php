<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
  public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 1024)->collate('utf8')->nullable();
            $table->string('name', 255)->collate('utf8');
            $table->float('price')->nullable();
            $table->float('shippingcost')->nullable();
            $table->integer('totalrating')->nullable();
            $table->string('thumbnail', 255)->nullable();
            $table->string('image', 255)->nullable();
            $table->float('discountpercentage')->nullable();
            $table->integer('votes')->nullable();
            $table->integer('idcategory')->unsigned();
            $table->timestamps();
            $table->foreign('idcategory')->references('id')->on('categories');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
