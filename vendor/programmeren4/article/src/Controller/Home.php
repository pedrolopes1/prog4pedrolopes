<?php
namespace Programmeren4\Article\Controller;

class Home extends \ModernWays\Mvc\Controller
{
    private $pdo;
    private $model;
    
    public function __construct(\ModernWays\Mvc\Route $route = null, \ModernWays\Dialog\Model\INoticeBoard $noticeBoard = null) {
        parent::__construct($route, $noticeBoard);
        $this->noticeBoard->startTimeInKey('PDO Connection');
        try {
        $this->pdo = new \PDO('mysql:host=localhost;dbname=pedrolopes;charset=utf8', 'pedrolopes101', '');
            $this->noticeBoard->setText('PDO connectie gelukt!');
            $this->noticeBoard->setCaption('PDO connectie voor Article');
        } catch (\Exception $e) {
            $this->noticeBoard->setText("{$e->getMessage()} op lijn {$e->getLine()} in bestand {$e->getFile()}");
            $this->noticeBoard->setCaption('PDO connectie voor Article');
            $this->noticeBoard->setCode($e->getCode());
        }
        $this->noticeBoard->log();        
        $this->model = new \Programmeren4\Article\Model\Article();
    } 
    
    public function Editing()
    {
        if ($this->pdo) {
            // het model vullen
            $command = $this->pdo->query("call ArticleSelectAll");
            // associatieve array kolomnamen en waarde per rij
            $this->model->setList($command->fetchAll(\PDO::FETCH_ASSOC));
            return $this->view('Home','Editing', $this->model);
          } else {
            return $this->view('Home', 'Error', null);
         }
    }
    
    public function Inserting() {
        return $this->view('Home', 'Inserting', null);
    }
    
    public function Insert() {
        if ($this->pdo) {
            $this->model->setName($_POST['ArticleName']);
            $this->model->setPurchaseDate($_POST['ArticlePurchaseDate']);
            $this->model->setPrice($_POST['ArticlePrice']);
            $statement = $this->pdo->prepare("call ArticleInsert(:pName, 
                :pPurchaseDate, :pPrice, @pId)");
            // bindValue is by reference
            // bindParam is by value
            $statement->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
            $statement->bindValue(':pPurchaseDate', $this->model->getPurchaseDate(), \PDO::PARAM_STR);
            $statement->bindValue(':pPrice', $this->model->getPrice(), \PDO::PARAM_STR);
            $result = $statement->execute();
            // zet de nieuw toegekende Id in het Id veld van het model
            // $this->model->setId($pdo->query('select @pId')->fetchColumn());
        }
        return $this->view('Home', 'Error', null);
     }
    
    public function Updating() {
        if ($this->pdo) {
            // derde parameter in het pad, is meestal een id
            $this->model->setId($this->route->getId());
            $statement = $this->pdo->prepare("call ArticleSelectOne(:pId)"); 
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $result = $statement->execute();
            $articleOne = $statement->fetch(\PDO::FETCH_ASSOC);
            $this->model->setName($articleOne['Name']);
            $this->model->setPurchaseDate($articleOne['PurchaseDate']);
            $this->model->setPrice($articleOne['Price']);
            return $this->view('Home','Updating', $this->model);
        } else {
            return $this->view('Home', 'Error', null);
        }
    }

}