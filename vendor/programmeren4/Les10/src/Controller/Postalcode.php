<?php
namespace Programmeren4\Les10\Controller;


class Postalcode extends \ModernWays\Mvc\Controller
{
    public function show()
    {
        $bestandpostcodes = file_get_contents('Les10/Postcodes.csv');
        $lines = explode("\n", $bestandpostcodes);
        $model = new \Programmeren4\Les10\Model\Postalcode();
        $postcodes = array();
        foreach ($lines as $line) {
            $postcodes[] =  explode('|', $line);
        }
        ;
        $model->setList($postcodes);
        return $this->view('Postalcode','Show', $model);
    }
}