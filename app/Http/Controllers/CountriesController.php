<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Country;
class CountriesController extends Controller
{
    public function index()
    {
        $country = Country::all();
        return view('countries.index', ['countries'=>$country]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
         $country = Country::all();
        return view('countries.create', ['countries'=>$country]);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {   $this->validate($request, [
            'code' => 'required|unique:countries|max:2',
            'name' => 'required|unique:countries|max:255',
            'latitude' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'longitude' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'shippingcostmultiplier' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ]);
       
        $country = new Country;
        $country->code =  $request->input('code');
        $country->latitude = $request->input('latitude');
        $country->longitude = $request->input('longitude');
        $country->shippingcostmultiplier = $request->input('shippingcostmultiplier');
        $country->name = $request->input('name');
    
        $country->save();
        return redirect('/countries');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
           $countries = Country::all();
        $country = Country::find($id);
        return view('countries.show', array('country'=>$country, 'countries'=>$countries));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {     $countries = Country::all();
        $country = Country::find($id);
        
        return view('countries.edit', array('country'=>$country, 'countries'=>$countries));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
          $this->validate($request, [
            'code' => 'required|unique:countries|max:2',
            'name' => 'required|unique:countries|max:255',
            'latitude' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'longitude' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'shippingcostmultiplier' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ]);
       
          $country = Country::find($id);
        $country->code =  $request->input('code');
        $country->latitude = $request->input('latitude');
        $country->longitude = $request->input('longitude');
        $country->shippingcostmultiplier = $request->input('shippingcostmultiplier');
        $country->name = $request->input('name');
    
        $country->save();
        return redirect('/countries/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $country = Country::find($id);
        $country->delete();
        return redirect('/countries');
        
    }
    
}
