<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;

class ProductsController extends Controller
{
    public function index() {
        $products = Product::all();
        return view('products.index', array('products' => $products));
    }
    
    public function create() {
        $categories = Category::all();
        $products = Product::all();
        
        return view('products.create', array('categories' => $categories, 'products' => $products));
    }
    
    public function store(Request $request) {
         $this->validate($request, [
            'description' => 'max:1024',
            'name' => 'required|max:255',
            'price' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'shippingcost' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'totalrating' => 'max:10|regex:/^[0-9]+?$/',
            'thumbnail' => 'max:255',
            'image' => 'max:255',
            'discountpercentage' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'votes' => 'max:11|regex:/^[0-9]+?$/',
            'idcategory' => 'required|max:10',
        ]);
        $product = new Product;
        $product->description = empty($request->input('description')) ? null : $request->input('description');
        $product->name = $request->input('name');
        $product->price = empty($request->input('price')) ? null : $request->input('price');
        $product->shippingcost = empty($request->input('shippingcost')) ? null : $request->input('shippingcost');
        $product->totalrating = empty($request->input('totalrating')) ? null : $request->input('totalrating');
        $product->thumbnail = empty($request->input('thumbnail')) ? null : $request->input('thumbnail');
        $product->image = empty($request->input('image')) ? null : $request->input('image');
        $product->discountpercentage = empty($request->input('discountpercentage')) ? null : $request->input('discountpercentage');
        $product->votes = empty($request->input('votes')) ? null : $request->input('votes');
        $product->idcategory = $request->input('idcategory');
        $product->save();
        
        return redirect('/products');
    }
    
    public function show($id) {
        $product = Product::find($id);
        $category = Category::find($product->idcategory);
        $products = Product::all();
        return view('products.show', array('product' => $product, 'category' => $category, 'products' => $products));
    }
    
    public function edit($id) {
        $product = Product::find($id);
        $category = Category::find($product->idcategory);
        $products = Product::all();
        $categories = Category::all();
        return view('products.edit', array('product' => $product, 'category' => $category, 'products' => $products, 'categories' => $categories));
    }
    
    public function update(Request $request, $id) {
     $this->validate($request, [
            'description' => 'max:1024',
            'name' => 'required|max:255',
            'price' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'shippingcost' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'totalrating' => 'max:10|regex:/^[0-9]+?$/',
            'thumbnail' => 'max:255',
            'image' => 'max:255',
            'discountpercentage' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'votes' => 'max:11|regex:/^[0-9]+?$/',
            'idcategory' => 'required|max:10',
        ]);
   
   
   
        $product = Product::find($id);
        $product->description = empty($request->input('description')) ? null : $request->input('description');
        $product->name = $request->input('name');
        $product->price = empty($request->input('price')) ? null : $request->input('price');
        $product->shippingcost = empty($request->input('shippingcost')) ? null : $request->input('shippingcost');
        $product->totalrating = empty($request->input('totalrating')) ? null : $request->input('totalrating');
        $product->thumbnail = empty($request->input('thumbnail')) ? null : $request->input('thumbnail');
        $product->image = empty($request->input('image')) ? null : $request->input('image');
        $product->discountpercentage = empty($request->input('discountpercentage')) ? null : $request->input('discountpercentage');
        $product->votes = empty($request->input('votes')) ? null : $request->input('votes');
        $product->idcategory = $request->input('idcategory');
        $product->save();
        return redirect('/products/'.$id);
    }
    
    public function destroy($id) {
        $product = Product::find($id);
        $product->delete(); 
        
        return redirect('/products');
    }
}
