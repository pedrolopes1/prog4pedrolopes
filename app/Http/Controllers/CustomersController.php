<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Customer;
use App\Country;

class CustomersController extends Controller
{
     public function index()
    {
        $customers = Customer::all();
        
        return view('customers.index', ['customers'=>$customers]);
    }
    


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {  $customers = Customer::all();
        $countries = Country::all();
        return view('customers.create', ['countries'=>$countries ,'customers'=>$customers ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nickname' => 'required|max:10',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'address1' => 'required|max:255',
            'address2' => 'required|max:255',
            'city' => 'required|max:255',
            'region' => 'max:80',
            'postalcode' => 'max:20',
            'phone' => 'required|max:40',
            'mobile' => 'required|max:40',
            'idcountry' => 'required|max:10',
        ]);
       
        $customer = new Customer;
        $customer->nickname = $request->input('nickname');
        $customer->firstname = $request->input('firstname');
        $customer->lastname = $request->input('lastname');
        $customer->address1 = $request->input('address1');
        $customer->address2 = $request->input('address2');
        $customer->city = $request->input('city');
        $customer->region = $request->input('region');
        $customer->postalcode = $request->input('postalcode');
        $customer->phone = $request->input('phone');
        $customer->mobile = $request->input('mobile');
        $customer->idcountry = $request->input('idcountry');
        $customer->save();
        return redirect('/customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {$customers = Customer::all();
        $customer = Customer::find($id);
        $country = Country::find($customer->idcountry);
        return view('customers.show', ['customer'=>$customer, 'country'=>$country, 'customers' => $customers]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        
        $customer = Customer::find($id);
        $customers = Customer::all();
            $country = Country::find($customer->idcountry);
        $countries = Country::all();
     
        return view('customers.edit', ['customer'=>$customer, 'countries'=>$countries , 'country'=>$country , 'customers' => $customers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
             $this->validate($request, [
            'nickname' => 'required|max:10',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'address1' => 'required|max:255',
            'address2' => 'required|max:255',
            'city' => 'required|max:255',
            'region' => 'max:80',
            'postalcode' => 'max:20',
            'phone' => 'required|max:40',
            'mobile' => 'required|max:40',
            'idcountry' => 'required|max:10',
        ]);
       
        $customer = Customer::find($id);
        $customer->nickname = $request->input('nickname');
        $customer->firstname = $request->input('firstname');
        $customer->lastname = $request->input('lastname');
        $customer->address1 = $request->input('address1');
        $customer->address2= $request->input('address2');
        $customer->city = $request->input('city');
        $customer->region = $request->input('region');
        $customer->postalcode = $request->input('postalcode');
        $customer->phone = $request->input('phone');
        $customer->mobile = $request->input('mobile');
        $customer->idcountry = $request->input('idcountry');
        $customer->save();
        return redirect('/customers/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        return redirect('/customers');
        
    }
    
}
