<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;

class CategoriesController extends Controller
{
    public function index() {
        $categories = Category::all();
        return view('categories.index', array('categories' => $categories));
    }
    
    public function create()
    {
           $category = Category::all();
        return view('categories.create' , ['categories'=>$category]);
    }
    
    public function store(Request $request) {
        $this->validate($request, [
            'name' =>  'max:1024',
            'description' => 'max:1024',
        ]);
        
        $category = new Category;
        $category->name = $request->input('name');
        $category->description = $request->input('description');
        $category->save();
        
        return redirect('/categories');
    }
    
    public function show($id) {
   $category = Category::find($id);
     $categories = Category::all();
   
        return view('categories.show', array('category'=>$category, 'categories'=>$categories));
    }
    
    public function edit($id) {
     $category = Category::find($id);
       $categories = Category::all();
        return view('categories.edit', array('category'=>$category, 'categories'=>$categories));
    }
    
    public function update(Request $request, $id) {
         $this->validate($request, [
            'name' => 'max:1024',
            'description' => 'max:1024',
        ]);
        
  $category = Category::find($id);
        $category->name = $request->input('name');
        $category->description = $request->input('description');
        $category->save();
        return redirect('/categories/'.$id);
    }
    
    public function destroy($id) {
        $category = Category::find($id);
        $category->delete(); 
        
        return redirect('/categories');
    }
}
