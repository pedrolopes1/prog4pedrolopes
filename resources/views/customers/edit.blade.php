@extends('layouts.master')

@section('content')
<section class="col-md-4">
    <form method="POST" action="{{ action('CustomersController@update', $customer->id) }}">
        
        {{ csrf_field() }}
        
        <input type="hidden" name="_method" value="PUT">
        
      <div class="form-group">
        <label for="nickname">Voornaam:</label> 
        <input type="text" name="nickname" class="form-control" id="nickname" value="{{ $customer->nickname }}"/>
    </div>
    
    <div class="form-group">
        <label for="firstname">First name:</label> 
        <input type="text" name="firstname" class="form-control" id="firstname" value="{{ $customer->firstname }}"/>
    </div>
    <div class="form-group">
        <label for="lastname">Last name:</label> 
        <input type="text" name="lastname" class="form-control" id="lastname" value="{{ $customer->lastname }}"/>
    </div>
    <div class="form-group">
        <label for="address1">Adress 1:</label> 
        <input type="text" name="address1" class="form-control" id="address1" value="{{ $customer->address1 }}"/>
    </div>
    <div class="form-group">
        <label for="address2">Adress 2:</label> 
        <input type="text" name="address2" class="form-control" id="address2"value="{{ $customer->address2 }}"/>
    </div>
    <div class="form-group">
        <label for="city">City:</label> 
        <input type="text" name="city" class="form-control" id="city" value="{{ $customer->city }}"/>
    </div>
    <div class="form-group">
        <label for="region">Region:</label> 
        <input type="text" name="region" class="form-control" id="region" value="{{ $customer->region }}"/>
    </div>
    <div class="form-group">
        <label for="postalcode">Postal code:</label> 
        <input type="text" name="postalcode" class="form-control" id="postalcode" value="{{ $customer->postalcode }}"/>
    </div>
    </section>
    
    <section  class="col-md-4">
    <div class="form-group">
    <label>Land</label><br/>
        <div >
        <select class="form-control" name="idcountry">
            <?php
            foreach($countries as $country)
            {
                echo '<option value="'.$country->id.'">'.$country->name.'</option> ';
            }
            ?>
        </select>
        </div>
    </div>
    <br/>
    <div class="form-group">
        <label for="phone">Phone:</label> 
        <input type="text" name="phone" class="form-control" id="phone"value="{{ $customer->phone }}"/>
    </div>
    
    <div class="form-group">
        <label for="mobile">Mobile:</label> 
        <input type="text" name="mobile" class="form-control" id="mobile" value="{{ $customer->mobile }}"/>
    </div>
    
    
    <button class="btn default">Opslaan</button>
            <a class="btn btn-default" href="{{action('CustomersController@index')}}">Annuleren</a>
        
    </form>   
</section>



 
<section class="col-md-4 bootcolor">

 <table class="table">
  <tr>
   <th>Id</th>
   <th>Bijnaam</th>
   <th>Voornaam</th>
   <th>Achternaam</th>
 
  </tr>
 
 <?php 
 foreach($customers as $customer){
  echo '<tr>';
  echo '<td>'.$customer->id.'</td>';
  echo '<td>'.$customer->nickname.'</td>'; 
  echo '<td>'.$customer->firstname.'</td>';
  echo '<td>'.$customer->lastname.'</td>';
    
  ?>
  
  <td>
   <form method="post" action="{{action('CustomersController@destroy', $customer->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
  </td>
  <td>
   <a href="{{ action('CustomersController@show', $customer) }}">show </a>
  </td>
  
  <?php
  echo '</tr>';
 }







 ?> 
 
 </table>
 </section>
@endSection