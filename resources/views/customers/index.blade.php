@extends('layouts.master')

@section('content')

<section class="col-md-8">
   <h1>Klanten Index</h1>
    <nav>
    <a class="btn btn-default" href="{{action('CustomersController@create')}}">Create</a>
      
 </nav>
    <br/>
</section>

        
  
<section class="col-md-4 bootcolor">

 <table class="table">
  <tr>
   <th>Id</th>
   <th>Bijnaam</th>
   <th>Voornaam</th>
   <th>Achternaam</th>
 
  </tr>
 
 <?php 
 foreach($customers as $customer){
  echo '<tr>';
  echo '<td>'.$customer->id.'</td>';
  echo '<td>'.$customer->nickname.'</td>'; 
  echo '<td>'.$customer->firstname.'</td>';
  echo '<td>'.$customer->lastname.'</td>';
    
  ?>
  
  <td>
   <form method="post" action="{{action('CustomersController@destroy', $customer->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
  </td>
  <td>
   <a href="{{ action('CustomersController@show', $customer) }}">show </a>
  </td>
  
  <?php
  echo '</tr>';
 }







 ?> 
 </<table>
  <tr>
   <td></td>
  </tr>
 </table>
 </section>@endsection