@extends('layouts.master')

@section('content')

   <section class="col-md-8"> 
  
    <title>Land id {{ $country->id }}</title>
    
    
    
    
    
    
    
    
  
       <a href="{{ action('CountriesController@edit', $country)  }}" class="btn btn-primary">Edit Country</a>
     
       
       
       
       <form class="btn btn-default" method="post" action="{{action('CountriesController@destroy', $country->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
    </form>
       
       <a class="btn btn-default" href="{{action('CountriesController@create')}}">Inserting</a>
       <a class="btn btn-default" href="{{action('CountriesController@index')}}">Cancel</a>
       
       
       
       
       
       
    <h1>Land {{ $country->id }}</h1>
    <ul>
      <li>Code: {{ $country->code }}</li>
      <li>Latitude: {{ $country->latitude }}</li>
       <li>Longitude: {{ $country->longitude }}</li>
      <li>Name: {{ $country->name }}</li>
      <li>Shippingcostmultiplier on: {{ $country->shippingcostmultiplier }}</li>
      
      
      
      
    </ul>
    </section>
    <section class="col-md-4 bootcolor">
<table class="table">
  <tr>
   <th>Id</th>
   <th>Code</th>
   <th>Latitude</th>
   <th>Longitude</th>
   <th>Name</th>
   <th>Shippingcost Multiplier</th>
  </tr>
 
<?php 
 foreach($countries as $c){
  echo '<tr>';
  echo '<td>'.$c->id.'</td>';
  echo '<td>'.$c->code .'</td>'; 
  echo '<td>'.$c->latitude.'</td>';
  echo '<td>'.$c->name.'</td>';
  echo '<td>'.$c->shippingcostmultiplier.'</td>';
  ?>
  <td>
   <form method="post" action="{{action('CountriesController@destroy', $country->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
   
  </td>
  <td>
   <a href="{{ action('CountriesController@show', $c) }}">show </a>
  </td>
  <?php
  echo '</tr>';
 }?>
 </table>
 </section>

 
@stop