@extends('layouts.master')

@section('content')


<form class="col-md-8" method="post" action="{{action('CountriesController@store')}}">
    <h2>Create</h2>
    {{csrf_field()}}
    
     <div class="form-group">
            <label for="code">Code:</label> 
            <input type="text" name="code" class="form-control" id="code"/>
        </div>
         <div class="form-group">
            <label for="latitude">Latitude:</label> 
            <input type="text" name="latitude" class="form-control" id="latitude"/>
        </div>
         <div class="form-group">
            <label for="longitude">Longitude:</label> 
            <input type="text" name="longitude" class="form-control" id="longitude"/>
        </div>
         <div class="form-group">
            <label for="name">Name:</label> 
            <input type="text" name="name" class="form-control" id="name"/>
        </div>
         <div class="form-group">
            <label for="shippingcostmultiplier">Shippingcost multiplier:</label> 
            <input type="text" name="shippingcostmultiplier" class="form-control" id="shippingcostmultiplier"/>
        </div>
<button class="btn default">opslaan</button>


</form>
 <section class="col-md-4 bootcolor" >
<table class="table">
  <tr>
   <th>Id</th>
   <th>Code</th>
   <th>Latitude</th>
   <th>Longitude</th>
   <th>Name</th>
   <th>Shippingcost Multiplier</th>
  </tr>
 
<?php 
 foreach($countries as $country){
  echo '<tr>';
  echo '<td>'.$country->id.'</td>';
  echo '<td>'.$country->code .'</td>'; 
  echo '<td>'.$country->latitude.'</td>';
  echo '<td>'.$country->name.'</td>';
  echo '<td>'.$country->shippingcostmultiplier.'</td>';
  ?>
  <td>
   <form method="post" action="{{action('CountriesController@destroy', $country->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
   
  </td>
  <td>
   <a href="{{ action('CountriesController@show', $country) }}">show </a>
  </td>
  <?php
  echo '</tr>';
 }?>
 </table>
 </section>
 
 
 
@endSection