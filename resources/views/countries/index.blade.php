@extends('layouts.master')

@section('content')



<section class="col-md-8">
 
    <h1>Landen Index</h1>
    <nav>
     <a class="btn btn-default" href="{{action('CountriesController@create')}}">Create</a>
 </nav>
    <br/>
</section>






 
 <section class="col-md-4 bootcolor">
<table class="table">
  <tr>
   <th>Id</th>
   <th>Code</th>
   <th>Latitude</th>
   <th>Longitude</th>
   <th>Name</th>
   <th>Shippingcost Multiplier</th>
  </tr>
 
<?php 
 foreach($countries as $country){
  echo '<tr>';
  echo '<td>'.$country->id.'</td>';
  echo '<td>'.$country->code .'</td>'; 
  echo '<td>'.$country->latitude.'</td>';
  echo '<td>'.$country->name.'</td>';
  echo '<td>'.$country->shippingcostmultiplier.'</td>';
  ?>
  <td>
   <form method="post" action="{{action('CountriesController@destroy', $country->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
   
  </td>
  <td>
   <a href="{{ action('CountriesController@show', $country) }}">show </a>
  </td>
  <?php
  echo '</tr>';
 }?>
 </table>
 </section>
 
 @stop