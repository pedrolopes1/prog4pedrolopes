@extends('layouts.master')

@section('content')
<section class="col-md-8">
    <form method="POST" action="{{ action('CountriesController@update', $country->id) }}">
        
        {{ csrf_field() }}
        
        <input type="hidden" name="_method" value="PUT">
        
        <div class="form-group">
            <label for="code">Code:</label> 
            <input type="text" name="code" class="form-control" id="code" value="{{ $country->code }}"/>
        </div>
        
      <div class="form-group">
            <label for="latitude">Latitude:</label> 
            <input type="text" name="latitude" class="form-control" id="latitude" value="{{ $country->latitude }}"/>
        </div>
        
         <div class="form-group">
            <label for="name">Name:</label> 
            <input type="text" name="name" class="form-control" id="name" value="{{ $country->name }}"/>
        </div>
        
         <div class="form-group">
            <label for="longitude">Longitude:</label> 
            <input type="text" name="longitude" class="form-control" id="longitude" value="{{ $country->longitude }}"/>
        </div>
        
        <div class="form-group">
            <label for="shippingcostmultiplier">shippingcostmultiplier:</label> 
            <input type="text" name="shippingcostmultiplier" class="form-control" id="shippingcostmultiplier" value="{{ $country->shippingcostmultiplier }}"/>
        </div>
        
     <button class="btn default">opslaan</button>
            <a class="btn btn-default" href="{{action('CustomersController@index')}}">Annuleren</a>

        
    </form>   
</table>

</section>

<section class="col-md-4 bootcolor">
<table class="table">
  <tr>
   <th>Id</th>
   <th>Code</th>
   <th>Latitude</th>
   <th>Longitude</th>
   <th>Name</th>
   <th>Shippingcost Multiplier</th>
  </tr>
 
<?php 
 foreach($countries as $c){
  echo '<tr>';
  echo '<td>'.$c->id.'</td>';
  echo '<td>'.$c->code .'</td>'; 
  echo '<td>'.$c->latitude.'</td>';
  echo '<td>'.$c->name.'</td>';
  echo '<td>'.$c->shippingcostmultiplier.'</td>';
  ?>
  <td>
   <form method="post" action="{{action('CountriesController@destroy', $country->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
   
  </td>
  <td>
   <a href="{{ action('CountriesController@show', $c) }}">show </a>
  </td>
  <?php
  echo '</tr>';
 }?>
 </table>
 </section>

@endSection