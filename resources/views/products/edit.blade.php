@extends('layouts.master')

@section('content')
<section class="col-md-4">
    <form method="POST" action="{{ action('ProductsController@update', $product->id) }}">
        
        {{ csrf_field() }}
        
        <input type="hidden" name="_method" value="PUT">
        
      <div class="form-group">
        <label for="description">Beschrijving:</label> 
        <input type="text" name="description" class="form-control" id="description" value="{{ $product->description }}"/>
    </div>
    
    <div class="form-group">
        <label for="name">Name:</label> 
        <input type="text" name="name" class="form-control" id="name" value="{{ $product->name }}"/>
    </div>
    <div class="form-group">
        <label for="price">Prijs:</label> 
        <input type="text" name="price" class="form-control" id="price" value="{{ $product->price }}"/>
    </div>
    <div class="form-group">
        <label for="shippingcost">Verzendskost:</label> 
        <input type="text" name="shippingcost" class="form-control" id="shippingcost" value="{{ $product->shippingcost }}"/>
    </div>
    <div class="form-group">
        <label for="totalrating">totale rating:</label> 
        <input type="text" name="totalrating" class="form-control" id="totalrating"value="{{ $product->totalrating }}"/>
    </div>
    <div class="form-group">
        <label for="thumbnail">Thumbnail:</label> 
        <input type="text" name="thumbnail" class="form-control" id="thumbnail" value="{{ $product->thumbnail }}"/>
    </div>
    <div class="form-group">
        <label for="image">Image:</label> 
        <input type="text" name="image" class="form-control" id="image" value="{{ $product->image }}"/>
    </div>
    <div class="form-group">
        <label for="discountpercentage">Kortings percentage:</label> 
        <input type="text" name="discountpercentage" class="form-control" id="discountpercentage" value="{{ $product->discountpercentage }}"/>
    </div>
     <div class="form-group">
        <label for="votes">Votes:</label> 
        <input type="text" name="votes" class="form-control" id="votes" value="{{ $product->votes }}"/>
    </div>
    </section>
    
    <section  class="col-md-4">
    <div class="form-group">
    <label>Categories</label><br/>
        <div >
        <select class="form-control" name="idcategory">
            <?php
            foreach($categories as $category)
            {
                echo '<option value="'.$category->id.'">'.$category->name.'</option> ';
            }
            ?>
        </select>
        </div>
    </div>
    <br/>

    
    
    <button class="btn default">Opslaan</button>
            <a class="btn btn-default" href="{{action('ProductsController@index')}}">Annuleren</a>
        
    </form>   
</section>



<section class="col-md-4 bootcolor">

 <table class="table">
  <tr>
   <th>Id</th>
   <th>Name</th>
   <th>Price</th>
   <th>Description</th>
 
  </tr>
 
 <?php 
 foreach($products as $product){
  echo '<tr>';
  echo '<td>'.$product->id.'</td>';
  echo '<td>'.$product->name.'</td>'; 
  echo '<td>'.$product->price.'</td>';
  echo '<td>'.$product->description.'</td>';
    
  ?>
  
  <td>
   <form method="post" action="{{action('ProductsController@destroy', $product->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
  </td>
  <td>
   <a href="{{ action('ProductsController@show', $product) }}">show </a>
  </td>
  
  <?php
  echo '</tr>';
 }







 ?> 
 
 </table>
 </section>
@endSection