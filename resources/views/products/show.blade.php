@extends('layouts.master')

@section('content')
 <section class="col-md-8"> 

    <title>Klant id {{ $product->id }}</title>
 
 
       <a href="{{ action('ProductsController@edit', $product)  }}" class="btn btn-primary">Edit Product</a>
         
       
       <form class="btn btn-default" method="post" action="{{action('ProductsController@destroy', $product->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
    </form>
       
       <a class="btn btn-default" href="{{action('ProductsController@create')}}">Inserting</a>
       <a class="btn btn-default" href="{{action('ProductsController@index')}}">Cancel</a>
       
       
       
       
       
       
    <h1>Product {{ $product->id }}</h1>
    <ul>
      <li>Description: {{ $product->description }}</li>
      <li>Name: {{ $product->name }}</li>
      <li>Price: {{ $product->price }}</li>
      <li>Verzendskost: {{ $product->shippingcost }}</li>
      <li>Totale rating: {{ $product->totalrating }}</li>
      <li>Thumbnail: {{ $product->thumbnail }}</li>
      <li>image: {{ $product->image }}</li>
      <li>Kortings percentage: {{ $product->discountpercentage }}</li>
      <li>Votes: {{ $product->votes }}</li>

     
      
    </ul>
    
    </section>
    
     
<section class="col-md-4 bootcolor">

 <table class="table">
  <tr>
   <th>Id</th>
   <th>Name</th>
   <th>Price</th>
   <th>Description</th>
 
  </tr>
 
 <?php 
 foreach($products as $product){
  echo '<tr>';
  echo '<td>'.$product->id.'</td>';
  echo '<td>'.$product->name.'</td>'; 
  echo '<td>'.$product->price.'</td>';
  echo '<td>'.$product->description.'</td>';
    
  ?>
  
  <td>
   <form method="post" action="{{action('ProductsController@destroy', $product->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
  </td>
  <td>
   <a href="{{ action('ProductsController@show', $product) }}">show </a>
  </td>
  
  <?php
  echo '</tr>';
 }







 ?> 
 
 </table>
 </section>
    
    
    
    
  @endSection
