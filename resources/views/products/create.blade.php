@extends('layouts.master')

@section('content')


<section class="col-md-4">
<form   method="post" action="{{action('ProductsController@store')}}">
    <h2>Create</h2>
    {{csrf_field()}}
    <div class="form-group">
        <label for="description">description:</label> 
        <input type="text" name="description" class="form-control" id="description"/>
    </div>
    <div class="form-group">
        <label for="name">name:</label> 
        <input type="text" name="name" class="form-control" id="name"/>
    </div>
    <div class="form-group">
        <label for="price">price:</label> 
        <input type="text" name="price" class="form-control" id="price"/>
    </div>
    <div class="form-group">
        <label for="shippingcost">shippingcost:</label> 
        <input type="text" name="shippingcost" class="form-control" id="shippingcost"/>
    </div>
    <div class="form-group">
        <label for="totalrating">totalrating:</label> 
        <input type="text" name="totalrating" class="form-control" id="totalrating"/>
    </div>
    <div class="form-group">
        <label for="thumbnail">Thumbnail:</label> 
        <input type="text" name="thumbnail" class="form-control" id="thumbnail"/>
    </div>
   
    
    
</section>
          
<section  class="col-md-4">
     </br>  </br>  </br>
     
      <div class="form-group">
        <label for="image">image:</label> 
        <input type="text" name="image" class="form-control" id="image"/>
    </div>
    <div class="form-group">
        <label for="discountpercentage">Discount Percentage:</label> 
        <input type="text" name="discountpercentage" class="form-control" id="discountpercentage"/>
    </div>
     
     
         <div class="form-group">
        <label for="votes">votes:</label> 
        <input type="text" name="votes" class="form-control" id="votes"/>
    </div>
    <div class="form-group">
    <label>Categorie</label><br/>
        <div >
        <select class="form-control" name="idcategory">
            <?php
            foreach($categories as $category)
            {
                echo '<option value="'.$category->id.'">'.$category->name.'</option>';
            }
            ?>
        </select>
        </div>
    </div>

  
    
    <button class="btn default">Opslaan</button>
        <a class="btn btn-default" href="{{action('ProductsController@index')}}">Annuleren</a>
    </form>
</section> 



<section class="col-md-4 bootcolor">

 <table class="table">
  <tr>
   <th>Id</th>
   <th>Name</th>
   <th>Price</th>
   <th>Description</th>
 
  </tr>
 
 <?php 
 foreach($products as $product){
  echo '<tr>';
  echo '<td>'.$product->id.'</td>';
  echo '<td>'.$product->name.'</td>'; 
  echo '<td>'.$product->price.'</td>';
  echo '<td>'.$product->description.'</td>';
    
  ?>
  
  <td>
   <form method="post" action="{{action('ProductsController@destroy', $product->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
  </td>
  <td>
   <a href="{{ action('ProductsController@show', $product) }}">show </a>
  </td>
  
  <?php
  echo '</tr>';
 }







 ?> 
 
 </table>
 </section>







@endSection