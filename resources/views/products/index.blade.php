@extends('layouts.master')

@section('content')

<section class="col-md-8">
   <h1>Producten Index</h1>
    <nav>
       <a class="btn btn-default" href="{{action('ProductsController@create')}}">Create</a>
 </nav>
    <br/>
</section>

        
  
<section class="col-md-4 bootcolor">

 <table class="table">
  <tr>
   <th>Id</th>
   <th>Name</th>
   <th>Price</th>
   <th>Description</th>
 
  </tr>
 
 <?php 
 foreach($products as $product){
  echo '<tr>';
  echo '<td>'.$product->id.'</td>';
  echo '<td>'.$product->name.'</td>'; 
  echo '<td>'.$product->price.'</td>';
  echo '<td>'.$product->description.'</td>';
    
  ?>
  
  <td>
   <form method="post" action="{{action('ProductsController@destroy', $product->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
  </td>
  <td>
   <a href="{{ action('ProductsController@show', $product) }}">show </a>
  </td>
  
  <?php
  echo '</tr>';
 }







 ?> 
 
 </table>
 </section>@endsection