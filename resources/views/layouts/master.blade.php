
<head>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ 'https://pfsl-prog4-pedrolopes101.c9users.io/css/style.css'}}">    
</head>



<body>
    <div class="wrapper">
        <div class="mikmakall">
            
                <nav class="nav">
                    <h1>Home</h1>
                </nav>
                <div class="mikamakheader">
                    <h1>MikMak</h1>
                </div>
               
            
        </div>


<main class="mikmakmain">
    @yield('content')
</main>


<footer class="footer">
     Pedro Lopes
</footer>

</div>
</body>

<script
src="https://code.jquery.com/jquery-3.1.1.min.js"
integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
crossorigin="anonymous">
</script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>