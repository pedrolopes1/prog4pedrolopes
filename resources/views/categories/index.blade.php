@extends('layouts.master')

@section('content')



<section class="col-md-8">
    <h1>Categories</h1>
    
    <nav>
                        <a class="btn btn-default" href="{{action('CategoriesController@create')}}">Create</a>
                </nav>
    <br/>
</section>






 
 <section class="col-md-4 bootcolor">
<table class="table">
  <tr>
   <th>Id</th>
   <th>Name</th>
   <th>Description</th>
   
  </tr>
 
<?php 
 foreach($categories as $category){
  echo '<tr>';
  echo '<td>'.$category->id.'</td>';
  echo '<td>'.$category->name .'</td>'; 
  echo '<td>'.$category->description.'</td>';

  ?>
  <td>
   <form method="post" action="{{action('CategoriesController@destroy', $category->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
   
  </td>
  <td>
   <a href="{{ action('CategoriesController@show', $category) }}">show </a>
  </td>
  <?php
  echo '</tr>';
 }?>
 </table>
 </section>
 
 @stop