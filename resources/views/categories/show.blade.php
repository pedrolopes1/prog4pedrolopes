@extends('layouts.master')

@section('content')

   <section class="col-md-8"> 
  
    <title>Categorie id {{ $category->id }}</title>
  
       <a href="{{ action('CategoriesController@edit', $category)  }}" class="btn btn-primary">Edit Country</a>
       
       
       
       <form class="btn btn-default" method="post" action="{{action('CategoriesController@destroy', $category->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
    </form>
       
       <a class="btn btn-default" href="{{action('CategoriesController@create')}}">Inserting</a>
       <a class="btn btn-default" href="{{action('CategoriesController@index')}}">Cancel</a>
       
       
       
    <h1>Categorie {{ $category->id }}</h1>
    <ul>
      <li>Code: {{ $category->name }}</li>
      <li>Category: {{ $category->description }}</li>

      
      
      
      
    </ul>
    </section>
    <section class="col-md-4 bootcolor">
<table class="table">
  <tr>
   <th>Id</th>

   <th>Name</th>
   <th>Category</th>
  </tr>
 
<?php 
 foreach($categories as $c){
  echo '<tr>';
  echo '<td>'.$c->id.'</td>';
  echo '<td>'.$c->name .'</td>'; 
  echo '<td>'.$c->description.'</td>';

  ?>
  <td>
   <form method="post" action="{{action('CategoriesController@destroy', $c->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
   
  </td>
  <td>
   <a href="{{ action('CategoriesController@show', $c) }}">show </a>
  </td>
  <?php
  echo '</tr>';
 }?>
 </table>
 </section>

 
@stop