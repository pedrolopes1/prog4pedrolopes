@extends('layouts.master')

@section('content')


<form class="col-md-8" method="post" action="{{action('CategoriesController@store')}}">
 <h2>Create</h2>
    {{csrf_field()}}
    
     <div class="form-group">
            <label for="name">Code:</label> 
            <input type="text" name="name" class="form-control" id="name"/>
        </div>
         <div class="form-group">
            <label for="description">description:</label> 
            <input type="text" name="description" class="form-control" id="description"/>
        </div>
         
<button class="btn default">opslaan</button>
    <a class="btn btn-default" href="{{action('CategoriesController@index')}}">Annuleren</a>
</form>



 <section class="col-md-4 bootcolor" >
<table class="table">
  <tr>
   <th>Id</th>
   <th>Name</th>
   <th>Description</th>
  </tr>
 
 
 
 
 
 
 
<?php 
 foreach($categories as $category){
  echo '<tr>';
  echo '<td>'.$category->id.'</td>';
  echo '<td>'.$category->name.'</td>'; 
  echo '<td>'.$category->description.'</td>';

  ?>
  <td>
   <form method="post" action="{{action('CategoriesController@destroy', $category->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
   
  </td>
  <td>
   <a href="{{ action('CategoriesController@show', $category) }}">show </a>
  </td>
  <?php
  echo '</tr>';
 }?>
 </table>
 </section>
 
 
 
@stop