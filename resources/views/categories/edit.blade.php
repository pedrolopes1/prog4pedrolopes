@extends('layouts.master')

@section('content')
<section class="col-md-8">
    <form method="POST" action="{{ action('CategoriesController@update', $category->id) }}">
        
        {{ csrf_field() }}
        
        <input type="hidden" name="_method" value="PUT">
     
        
         <div class="form-group">
            <label for="name">Name:</label> 
            <input type="text" name="name" class="form-control" id="name" value="{{ $category->name }}"/>
        </div>
        
         <div class="form-group">
            <label for="longitude">Longitude:</label> 
            <input type="text" name="description" class="form-control" id="description" value="{{ $category->description }}"/>
        </div>
        
       <a class="btn btn-default" href="{{action('CategoriesController@destroy', $category->id) }}">Annuleren</a>
        
 <button class="btn default">Opslaan</button>
               
        
    </form>   
</table>

</section>

<section class="col-md-4 bootcolor">
<table class="table">
  <tr>
   <th>Id</th>
   <th>Name</th>
   <th>Description</th>
  </tr>
 
<?php 
 foreach($categories as $c){
  echo '<tr>';
  echo '<td>'.$c->id.'</td>';
  echo '<td>'.$c->name .'</td>'; 
  echo '<td>'.$c->description.'</td>';

  ?>
  <td>
   <form method="post" action="{{action('CategoriesController@destroy', $c->id) }}">
    <input type="hidden" name="_method" value="DELETE"/>
    {{csrf_field()}}
    <button>Delete</button>
   </form>
   
  </td>
  <td>
   <a href="{{ action('CategoriesController@show', $c) }}">show </a>
  </td>
  <?php
  echo '</tr>';
 }?>
 </table>
 </section>

@endSection