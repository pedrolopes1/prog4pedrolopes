@extends('layouts.master')

@section('content')


<section class="eersterij" id="target">
    <section class="product"><a href="{{action('ProductsController@index')}}">Product</a> </section>
     <section class="supplier"><a href="">Supplier</a> </section>
      <section class="foto1"><img border="0" alt="W3Schools" src="http://www.jprarts.com/wp-content/uploads/2011/03/Webshop.jpg" width="100%" height="100%"></section>
  
</section>

<section class="tweederij">
    <section class="customer"><a href="{{action('CustomersController@index')}}">Customer</a> </section>
	 <section class="foto2"><img border="0" alt="W3Schools" src="http://bestellen.sho.nl/images/webshop.png" width="100%" height="100%"></section>
     <section class="order"><a href="">Order</a> </section>
      <section class="orderitems"><a href="">Order Items</a> </section>
  
</section>
<section class="derderij">
    <section class="foto3"><img border="0" alt="W3Schools" src="http://www.burgman-security.nl/wp-content/uploads/2016/08/webshop-burgman-security.png" width="100%" height="100%"></section>
	 <section class="country"><a href="{{action('CountriesController@index')}}">Countries</a> </section>
     <section class="orderstatus"><a href="">Status</a> </section>
      <section class="category"><a href="{{action('CategoriesController@index')}}">Category</a> </section>
  
</section>

@endsection 